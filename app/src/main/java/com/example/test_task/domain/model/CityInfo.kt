package com.example.test_task.domain.model

data class CityInfo(
    val cityName: String,
    val countryName: String,
    val imageUrl: String,
    val description: String
)