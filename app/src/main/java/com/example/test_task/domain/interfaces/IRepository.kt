package com.example.test_task.domain.interfaces

import com.example.test_task.domain.model.CityInfo
import io.reactivex.Single

interface IRepository {

    fun getCities(): Single<List<CityInfo>>

    fun getCity(cityName: String): Single<CityInfo>

}