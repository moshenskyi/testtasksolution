package com.example.test_task.domain.interfaces

interface IUseCase<T : IUseCase.Parameters, R> {

    fun build(parameters: T) : R

    interface Parameters

}
