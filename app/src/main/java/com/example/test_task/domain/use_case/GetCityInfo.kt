package com.example.test_task.domain.use_case

import com.example.test_task.domain.interfaces.IRepository
import com.example.test_task.domain.interfaces.IUseCase
import com.example.test_task.domain.model.CityInfo
import io.reactivex.Single

interface IGetCityInfo : IUseCase<GetCityInfo.Parameters, Single<CityInfo>>

class GetCityInfo(private val repository: IRepository) : IGetCityInfo {

    override fun build(parameters: Parameters): Single<CityInfo> {
        return repository.getCity(parameters.cityName)
    }

    data class Parameters(val cityName: String) : IUseCase.Parameters

}
