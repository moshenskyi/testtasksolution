package com.example.test_task.domain.use_case

import com.example.test_task.domain.interfaces.IRepository
import com.example.test_task.domain.interfaces.IUseCase
import com.example.test_task.domain.model.CityInfo
import io.reactivex.Single

interface IGetCityList : IUseCase<AbsentParameters, Single<List<CityInfo>>>

class GetCityList(private val repository: IRepository) : IGetCityList {

    override fun build(parameters: AbsentParameters): Single<List<CityInfo>> {
        return repository.getCities()
    }

}