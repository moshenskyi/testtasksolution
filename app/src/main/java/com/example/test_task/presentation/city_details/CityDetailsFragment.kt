package com.example.test_task.presentation.city_details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.test_task.R
import com.example.test_task.presentation.ViewModelFactory

private const val CITY_NAME = "city_name"

class CityDetailsFragment : Fragment() {
    private lateinit var cityName: String
    private lateinit var cityDetails: TextView

    private lateinit var viewModel: CityDetailsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requireArguments().let {
            cityName = it.getString(CITY_NAME, "")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_city_details, container, false)
        cityDetails = root.findViewById(R.id.city_details)
        return root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this, ViewModelFactory()).get(CityDetailsViewModel::class.java)
        viewModel.result.observe(viewLifecycleOwner, Observer { cityDetails.text = it })
        viewModel.errorMessage.observe(
            viewLifecycleOwner,
            Observer { Toast.makeText(activity, it, Toast.LENGTH_LONG).show() })
    }

    override fun onStart() {
        super.onStart()
        viewModel.getCityByName(cityName)
    }

    companion object {
        fun newInstance(cityName: String) =
            CityDetailsFragment().apply {
                arguments = Bundle().apply {
                    putString(CITY_NAME, cityName)
                }
            }
    }
}
