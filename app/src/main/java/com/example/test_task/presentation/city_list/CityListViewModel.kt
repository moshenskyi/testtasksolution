package com.example.test_task.presentation.city_list

import com.example.test_task.domain.model.CityInfo
import com.example.test_task.domain.use_case.AbsentParameters
import com.example.test_task.domain.use_case.IGetCityList
import com.example.test_task.presentation.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class CityListViewModel(private val getCityList: IGetCityList) : BaseViewModel<List<CityInfo>>() {

    fun getCityList() {
        addDisposable(
            getCityList.build(AbsentParameters())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { result -> _result.postValue(result) },
                    { error -> _errorMessage.postValue(error.message) }
                )
        )
    }
}
