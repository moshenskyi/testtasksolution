package com.example.test_task.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.test_task.R
import com.example.test_task.presentation.city_list.CityListFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.container, CityListFragment.newInstance())
                .commit()
        }
    }
}
