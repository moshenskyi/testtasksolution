package com.example.test_task.presentation.city_details

import com.example.test_task.domain.use_case.GetCityInfo
import com.example.test_task.domain.use_case.IGetCityInfo
import com.example.test_task.presentation.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class CityDetailsViewModel(private val getCityInfo: IGetCityInfo) : BaseViewModel<String>() {

    fun getCityByName(cityName: String) {
        addDisposable(
            getCityInfo.build(GetCityInfo.Parameters(cityName))
                .map { result -> result.description }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { result -> _result.postValue(result) },
                    { error -> _errorMessage.postValue(error.message) }
                )
        )
    }
}