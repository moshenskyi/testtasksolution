package com.example.test_task.presentation.city_list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout.VERTICAL
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.test_task.R
import com.example.test_task.domain.model.CityInfo
import com.example.test_task.presentation.ViewModelFactory
import com.example.test_task.presentation.city_details.CityDetailsFragment

class CityListFragment : Fragment(), CityListAdapter.OnItemSelectedListener {

    private lateinit var viewModel: CityListViewModel

    private lateinit var adapter: CityListAdapter
    private lateinit var cityList: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_city_list, container, false)
        initRecycler(root)
        return root
    }

    private fun initRecycler(root: View) {
        cityList = root.findViewById(R.id.city_list)
        cityList.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)

        adapter = CityListAdapter(this)
        cityList.adapter = adapter

        cityList.addItemDecoration(DividerItemDecoration(activity, VERTICAL))
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this, ViewModelFactory()).get(CityListViewModel::class.java)
        viewModel.result.observe(viewLifecycleOwner, Observer { adapter.submitItems(it) })
        viewModel.errorMessage.observe(
            viewLifecycleOwner,
            Observer { Toast.makeText(activity, it, Toast.LENGTH_LONG).show() })
    }

    override fun onStart() {
        super.onStart()
        viewModel.getCityList()
    }

    companion object {
        fun newInstance() = CityListFragment()
    }

    override fun onItemSelected(selectedItem: CityInfo) {
        activity?.let {
            it.supportFragmentManager
                .beginTransaction()
                .addToBackStack(null)
                .replace(R.id.container, CityDetailsFragment.newInstance(selectedItem.cityName))
                .commit()
        }
    }
}
