package com.example.test_task.presentation.city_list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.test_task.R
import com.example.test_task.domain.model.CityInfo

class CityListAdapter(private val listener: OnItemSelectedListener) : RecyclerView.Adapter<CityViewHolder>() {
    private val cityList: MutableList<CityInfo> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CityViewHolder {
        val itemView =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.city_list_item, parent, false)
        return CityViewHolder(itemView, listener)
    }

    override fun getItemCount() = cityList.size

    override fun onBindViewHolder(holder: CityViewHolder, position: Int) {
        holder.bind(cityList[position])
    }

    fun submitItems(cityList: List<CityInfo>) {
        this.cityList.clear()
        this.cityList.addAll(cityList)
        notifyDataSetChanged()
    }

    interface OnItemSelectedListener {
        fun onItemSelected(selectedItem: CityInfo)
    }
}