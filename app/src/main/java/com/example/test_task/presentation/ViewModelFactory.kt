package com.example.test_task.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.test_task.data.Repository
import com.example.test_task.domain.use_case.GetCityInfo
import com.example.test_task.domain.use_case.GetCityList
import com.example.test_task.presentation.city_details.CityDetailsViewModel
import com.example.test_task.presentation.city_list.CityListViewModel

class ViewModelFactory : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass == CityListViewModel::class.java) {
            return CityListViewModel(GetCityList(Repository())) as T
        }
        return CityDetailsViewModel(GetCityInfo(Repository())) as T
    }
}