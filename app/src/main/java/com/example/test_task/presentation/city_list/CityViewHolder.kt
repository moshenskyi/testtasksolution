package com.example.test_task.presentation.city_list

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.test_task.R
import com.example.test_task.domain.model.CityInfo

class CityViewHolder(
    itemView: View,
    private val listener: CityListAdapter.OnItemSelectedListener
) : RecyclerView.ViewHolder(itemView) {
    private val cityImage: ImageView = itemView.findViewById(R.id.city_image)
    private val cityTitle: TextView = itemView.findViewById(R.id.city_title)
    private val countryTitle: TextView = itemView.findViewById(R.id.country_title)
    private val cityDetails: TextView = itemView.findViewById(R.id.city_details)

    fun bind(cityInfo: CityInfo) {
        itemView.setOnClickListener { listener.onItemSelected(cityInfo) }

        Glide.with(cityImage)
            .load(cityInfo.imageUrl)
            .centerInside()
            .into(cityImage)

        cityTitle.text = cityInfo.cityName
        countryTitle.text = cityInfo.countryName
        cityDetails.text = cityInfo.description
    }

}
