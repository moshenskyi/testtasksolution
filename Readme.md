# Libs
- Glide
- RxJava
- RxAndroid

# Implementation
App contains 2 screens: `City List` and `City Details`

### City List
Gets list from `Repository` during `onStart` event. Repository has predefined list of cities with information about it. It returns cities which `CityListViewModel` propagates to fragment. Items are pushed to `adapter` which binds each of it via `ViewHolder`. `ViewHolder` gets additional parameter - `click listener` which fires when any of items is clicked and navigates user to `City Details`

##### CityList data
It contains image which is obtained via `Glide`, city and country name, short description(it's one line `TextView` with `ellipsize` attribute set to `marqueue` to show that it's not the end of description and put 3 dots in the end of `TextView`)

### City Details
Gets city name as argument and obtains desired item from `Repository` by city name.

# Layers
- Domain(contains `UseCases`, interface declaration for `Repository`, models)
- Data(contans `Repository` implementation)
- Presentation(contains `adapters`, `fragments` and `activity`, `ViewModels` - organized MVVM on this layer)